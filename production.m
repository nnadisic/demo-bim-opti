c = [6 6 6]'; 
A = [1 6 3; 3 1 3; 1 3 2; 2 3 4];
b = [84 42 21 42]';
lb = [0 0 0]';
ub = [];
ctype = "UUUU";
vtype = "CCC";
%vtype = "III";
sense = -1;

[xmin,fmin] = glpk(c,A,b,lb,ub,ctype,vtype,sense)