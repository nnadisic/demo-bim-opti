using JuMP
using GLPK

# Data
coefx = [500 450 435]
coefy = [5000 4000 6000]

# Optimization model
model = Model(GLPK.Optimizer)

@variable(model, x[1:3] >= 0)
@variable(model, y[1:3], Bin)

@objective(model, Min, sum(coefx*x) + sum(coefy*y))

@constraints(model, begin
             sum(x) == 1050
             x[1] <= 500 * y[1]
             x[2] <= 900 * y[2]
             x[3] <= 400 * y[3]
             end)

JuMP.optimize!(model)

# Display results
xmin = JuMP.value.(x)
display(xmin)
ymin = JuMP.value.(y)
display(ymin)
display(objective_value(model))
