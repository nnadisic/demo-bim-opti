using JuMP
using GLPK

model = Model(GLPK.Optimizer)

@variable(model, x[1:9, 1:9, 1:9], Bin)

@constraints(model, begin
             # Constraint 1 - Only one value appears in each cell
             cell[i in 1:9, j in 1:9], sum(x[i, j, :]) == 1
             # Constraint 2 - Each value appears in each row once only
             row[i in 1:9, k in 1:9], sum(x[i, :, k]) == 1
             # Constraint 3 - Each value appears in each column once only
             col[j in 1:9, k in 1:9], sum(x[:, j, k]) == 1
             # Constraint 4 - Each value appears in each 3x3 subgrid once only
             subgrid[i=1:3:7, j=1:3:7, val=1:9], sum(x[i:i + 2, j:j + 2, val]) == 1
             end)

# @constraints(model, begin
#              x[1,1,9] == 1
#              x[1,2,8] == 1
#              end)

JuMP.optimize!(model)

mip_solution = JuMP.value.(x)
sol = zeros(Int, 9, 9)
for row in 1:9, col in 1:9, val in 1:9
    if mip_solution[row, col, val] >= 0.9
        sol[row, col] = val
    end
end

display(sol)
